# jquery-email-validation
An email address validation jQuery Plugin

#Dependency
jQuery 1.11

#Usage

```
#!javascript

$("input").isValid();
```

#With parameters

```
#!javascript

$("input").isValid({"color":"whitesmoke","background":"orange"});
```
