(function($) {
	$.fn.extend({
	isValid: function(options)	{
		var settings = $.extend({
            color: "red",
            background: "white"
        }, options );
		return this.each(function() {
				var oldColor = $(this).css("color"),
					oldBackgroundColor = $(this).css("background");
				$(this).keyup(function (){
					var str = $(this).val();
					var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					var result = emailPattern.test(str);
					if(!result){
						$(this).css({"color":settings.color,"background":settings.background});
					}
					else{
						$(this).css({"color":oldColor,"background":oldBackgroundColor});
					}
				});
			});
		}
	});
})(jQuery);
